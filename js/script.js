// Sections elements
var elementHome = document.getElementById('home-content');
var elementAbout = document.getElementById('about-content');
var elementSkills = document.getElementById('skills');
var elementProjects = document.getElementById('projects-content');
var elementContact = document.getElementById('contact-content');

// Home section element
var elementHello = document.getElementById('hello');
var elementName = document.getElementsByClassName('name');
var elementProfession = document.getElementById('profession-title');
var elementSocials = document.getElementsByClassName('socials-link');
var elementResume = document.getElementById('resume');

// About section elements
var elementAboutText = document.getElementById('carousel-text');
var elementAboutImage = document.getElementById('about-image');
var aboutCarousel = document.querySelector('#carouselIndicators');

// Skill section elements
var elementSkillsText = document.getElementById('skills-text');
var elementSkillsImage = document.getElementById('skills-image');
var elementSkillsSet = document.getElementsByClassName('skills-set');

// Projects section elements
var elementProjectsText = document.getElementById('projects-title');
var elementProjectSet = document.getElementsByClassName('card');
var elementProjectCarousel = document.getElementById('carouselProjectIndicators');
var projectCarousel = document.querySelector('#carouselProjectIndicators');

// Set listener to hide the DOM objects on load
window.addEventListener('DOMContentLoaded', setAllHidden);

// listen for scroll event and call animate function
document.addEventListener('scroll', animate);

/**
 * 
 */
function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

//usage:
//readTextFile("./js/data.json", function(text){var data = JSON.parse(text);console.log(data);});
/**
 * Checks if element is in view
 * @param {*} element element to check if in view
 * @param {*} threshold Threshold to anticipete the view and trigger animations
 */
function inView(element, threshold) {
  // get element height
  var elementHeight = element.clientHeight;
  // get window height
  var windowHeight = window.innerHeight;
  // get number of pixels that the document is scrolled
  var scrollY = window.scrollY || window.pageYOffset;
  
  // get current scroll position (distance from the top of the page to the bottom of the current viewport)
  var scrollPosition = scrollY + windowHeight;
  // get element position (distance from the top of the page to the bottom of the element)
  var elementPosition = element.getBoundingClientRect().top + scrollY + elementHeight - threshold;
  
  // is scroll position greater than element position? (is element in view?)
  if (scrollPosition > elementPosition) {
    return true;
  }
  
  return false;
}

/**
 * Set every element to animate hidden
 */
function setAllHidden(){
    
    // Sections elements
    elementHome.classList.add('hidden');
    elementAbout.classList.add('hidden');
    elementSkills.classList.add('hidden');
    elementProjects.classList.add('hidden');
    elementContact.classList.add('hidden');
    
    // Home section element
    elementHello.classList.add('hidden');
    animationSeq(elementName,0, 'hidden', false);
    elementProfession.classList.add('hidden');
    animationSeq(elementSocials,0, 'hidden', false);
    elementResume.classList.add('hidden');
    
    // About section elements
    animateCarousel(aboutCarousel, false);
    elementAboutText.classList.add('hidden');
    elementAboutImage.classList.add('hidden');
    
    // Skill section elements
    elementSkillsText.classList.add('hidden');
    elementSkillsImage.classList.add('hidden');
    animationSeq(elementSkillsSet,0, 'hidden', false);
    
    // Projects section elements
    animateCarousel(projectCarousel, false);
    elementProjectsText.classList.add('hidden');
    animationSeq(elementProjectSet,0, 'hidden', false);
    elementProjectCarousel.classList.add('hidden');

    // Activate the first section animation
    setTimeout(function() {
        animateHome();
    }, 1200);
}

/**
 * animate element when it is in view
 */
function animate() {
  if (inView(elementHome, 0)) {
    animateHome();
  } 
  if (inView(elementAbout,300)) {
    animateAbout();
  } 
  if (inView(elementSkills, 750)) {
    animateSkills();
  } 
  if (inView(elementProjects, 800)) {
    animateProjects();
  }
  if (inView(elementContact, 1000)) {
    animateContact();
  }
}

/**
 * Set the sequences of animations on the elements in the 
 * home section.
 */
function animateHome() {
    // Show content home section
    elementHome.classList.remove('hidden');
    elementHome.classList.add('fadeAnimation');

    // Set animation first elements
    elementHello.classList.remove('hidden');
    elementHello.classList.add('panAnimation');
    elementProfession.classList.remove('hidden');
    elementProfession.classList.add('panAnimation');

    // Set sequence on main headings
    setTimeout(function() {
        animationSeq(elementName, 400, 'fadeAnimation', true);
    }, 100);
    // Set sequence on links icons 
    setTimeout(function() {
        animationSeq(elementSocials, 200, 'riseSocial', true);
    }, 300);

    // last element download button
    setTimeout(function() {
        elementResume.classList.remove('hidden');
        elementResume.classList.add('fadeAnimation');
    }, 1500);
}

/**
 * Set the sequences of animations on the elements in the 
 * about section.
 */
function animateAbout() {
    // Show content about section
    elementAbout.classList.remove('hidden');
    elementAbout.classList.add('fadeAnimation');

    // Set animation text elements
    setTimeout(function() {
        elementAboutText.classList.remove('hidden');
        elementAboutText.classList.add('riseAnimation');
        animateCarousel(aboutCarousel, 5000);
    }, 300);

    // Set animation image elements
    setTimeout(function() {
        elementAboutImage.classList.remove('hidden');
        elementAboutImage.classList.add('riseAnimation');
    }, 600);

}

/**
 * Set the sequences of animations on the elements in the 
 * skills section.
 */
function animateSkills() {
    // Show content skills section
    elementSkills.classList.remove('hidden');
    elementSkills.classList.add('fadeAnimation');

    // Set animation image elements
    setTimeout(function() {
        elementSkillsImage.classList.remove('hidden');
        elementSkillsImage.classList.add('riseAnimation');
    }, 200);

    // Set animation text section
    setTimeout(function() {
        elementSkillsText.classList.remove('hidden');
        elementSkillsText.classList.add('riseAnimation');
    }, 400);

    // Set animation each skill's row
    setTimeout(function() {
        animationSeq(elementSkillsSet, 200, 'panAnimationSkills', true);
    }, 600); 
}

/**
 * Set the sequences of animations on the elements in the 
 * projects section.
 */
function animateProjects() {
    
    // Show content projects section
    setTimeout(function() {
        elementProjects.classList.remove('hidden');
        elementProjects.classList.add('fadeAnimation');
    }, 100);

    // Show projects title animation
    setTimeout(function() {
        elementProjectsText.classList.remove('hidden');
        elementProjectsText.classList.add('fadeAnimation');
    }, 200);

    // Set animation text elements
    setTimeout(function() {
        elementProjectCarousel.classList.remove('hidden');
        elementProjectCarousel.classList.add('fadeAnimation');
    }, 400);

    // Set animation each porject's card
    setTimeout(function() {
        animationSeq(elementProjectSet, 400, 'riseProjects', true);
        animateCarousel(projectCarousel, 5000);
    }, 600); 

}

/**
 * Set the sequences of animations on the elements in the 
 * contact section. 
 */
function animateContact() {
    elementContact.classList.remove('hidden');
    elementContact.classList.add('fadeAnimation');
}

/**
 * 
 * @param {*} object 
 * @param {*} pause 
 */
function animateCarousel(object, pause) {
    var carousel = new bootstrap.Carousel(object, {
        interval: pause
    });
}

/**
 * Trigger icons animation sequence.
 * @param {*} arr array of DOM elements to animate.
 * @param {*} timing delay between sequences. 
 * @param {*} animation type of animation.
 * @param {*} hidden true if the element is hidden, otherwise false.
 */
function animationSeq(arr, timing, animation, hidden){
    
    var i = 0;
    Array.prototype.forEach.call(arr, function(element) {
        i++;
        setTimeout(function() {
            element.classList.add(animation);
            if (hidden) {
                element.classList.remove('hidden');
            }
        }, timing * i);
    });
}