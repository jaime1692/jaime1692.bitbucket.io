// Disabling form submissions if there are invalid fields
(function () {
    'use strict'
  
    // Fetch the  contact form
    var forms = document.querySelectorAll('.needs-validation')
  
    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }
  
          form.classList.add('was-validated')
        }, false)
      })
  })()


/* 
API send email - disable
*/
$('#contact-form').click(function() {
    window.open('mailto:jaimeg1606@gmail.com?subject=Ref: portfolio contact&body=');

    /*
    var email = $("#inputEmail").val();
    var subject = $("#inputSubject").val();
    var content = $("#inputContent").val();
    
    console.log("email = " + email);
    console.log("subject = " + subject);
    console.log("content = " + content);
    $.ajax({
        type: "POST",
        url: "https://mandrillapp.com/api/1.0/messages/send.json",
        data: {
        'key': '',
        'message': {
            'from_email': 'jaimeg1606@gmail.com',
            'to': [
                {
                'email': 'jaimeg1606@gmail.com',
                'type': 'to'
                },
                {
                'email': email,
                'type': 'to'
                }
            ],
            'autotext': 'true',
            'subject': subject,
            'html': content
        }
        }
    }).done(function(response) {
        console.log(response);
    });
    */
});
